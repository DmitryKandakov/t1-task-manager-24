package ru.t1.dkandakov.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityException {

    public ModelNotFoundException() {
        super("Error! Model wasn't found...");
    }

}
