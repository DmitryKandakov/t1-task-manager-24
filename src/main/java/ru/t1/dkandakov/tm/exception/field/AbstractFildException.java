package ru.t1.dkandakov.tm.exception.field;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractFildException extends AbstractException {

    public AbstractFildException(@NotNull final String message) {
        super(message);
    }

    public AbstractFildException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractFildException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractFildException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}