package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-status-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project's status by index.";
    }

}