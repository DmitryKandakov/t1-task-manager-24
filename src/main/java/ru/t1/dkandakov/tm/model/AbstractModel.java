package ru.t1.dkandakov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Setter
@Getter
public abstract class AbstractModel {

    @Getter
    @Setter
    @NotNull
    protected String id = UUID.randomUUID().toString();

}